Three realizations of SPI protocol for AVR MCUs - hardware, software and using USI
This is only Master version.

# CAUTION
I don't guarantee that this code will work on your configuration. I tested it on ATmega8 @ 8MHz and ATtiny24V @ 1MHz

# CAUTION 2
I added #error lines in each libSPI.h - it's reminder that you need to configure pins in defines. Comment this line when you configured it

# CAUTION 3
Expected that all .h files will be in /inc/ folder and all .cpp files in /lib/ folder. If you want to change it, you will need to manually correct include path in both files

# Functions
All versions have the same syntax and functionality.

#### `spi_init (void) : void`
Initializes SPI.
#### `spi_fast_shift (uint8_t data) : uint8_t`
Sends and receives one byte of data.
#### `spi_transmit_sync (uint8_t *dataout, uint8_t len) : void`
Sends array of data. First parameter - pointer to array, second - length.
#### `spi_transfer_sync (uint8_t *dataout, uint8_t *datain, uint8_t len) : void`
Sends and receives array of data. First parameter - pointer to outgoing data array that will be sent), second - pointer to incoming (read) data array, and third - length.

# Structure
Each library splitted to .h and .cpp files. I did it because it solves multi-definition problem occured when library included in several .cpp files

# Versions

## Harware
This version uses hardware SPI port of the MCU and operates with SPCR, SPSR and SPDR registers. 
Library will work only in case MCU have hardware SPI

## Software
Some MCUs doesn't have hardware SPI. For them I wrote software variant.
This version don't use anything hardware, it's fully standalone and may be used on any MCU.

## USI
Some MCUs don't have SPI, but have USI. It's possible to create SPI on USI base
This version will work only if MCU have hardware USI.
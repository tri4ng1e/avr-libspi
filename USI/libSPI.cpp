#include "../inc/libSPI.h"

void spi_init(){
	SPI_PORT&=~(1<<SPI_MISO|1<<SPI_MOSI|1<<SPI_SCK);
	SPI_DDR|=1<<SPI_MOSI|1<<SPI_SCK;
	SPI_DDR&=~(1<<SPI_MISO);

	USICR=1<<USIWM0|0<<USICS0;
}

uint8_t spi_fast_shift(uint8_t dataout){
	USIDR=dataout;
	for (uint8_t i=0; i<8; i++){
		USICR=(1<<USIWM0)|(0<<USICS0)|(1<<USITC);
		USICR=(1<<USIWM0)|(0<<USICS0)|(1<<USITC)|(1<<USICLK);
	}
	return USIDR;
}

void spi_transfer_sync(uint8_t *dataout, uint8_t *datain, uint8_t len){
	for (uint8_t i=0; i<len; i++){
		datain[i]=spi_fast_shift(dataout[i]);
	}
}

void spi_transmit_sync(uint8_t *dataout, uint8_t len){
	for (uint8_t i=0; i<len; i++){
		spi_fast_shift(dataout[i]);
	}
}

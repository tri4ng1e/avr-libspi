/*
		libSPI.h - header
	Author:	Sky-WaLkeR
	
	Version: 0.4
	   Date: 20.06.2013 18:16

	Description:
  This library allows you to use the USI as SPI. 
  There is no SPI on some MCUs (like AtTiny24). You can emulte a SPI work using this lib

	Depends:
  None

	Notes:
  This file must be in $(project_folder)\inc folder (like "spi_test\inc\libSPI.h")
  File libSPI.cpp must be in $(project_folder)\lib folder (like "spi_test\lib\libSPI.cpp")
  If you don't like this - change path in libSPI.cpp file

  WARNING! This library not tested well...
  
*/
#ifndef _LIB_SPI_USI_H__
#define _LIB_SPI_USI_H__

#include <avr/io.h>

// Pin settings
// Comment first line to remove error
#error You may forget to configure pins! (libSPI.h)
#define SPI_PORT PORTA
#define SPI_DDR  DDRA
#define SPI_MOSI 5
#define SPI_MISO 6
#define SPI_SCK  4

void spi_init();

// send and receive one byte
uint8_t spi_fast_shift(uint8_t dataout);

// send and receive array of bytes
void spi_transfer_sync(uint8_t *dataout, uint8_t *datain, uint8_t len);

// send array of bytes without receiving
void spi_transmit_sync(uint8_t *dataout, uint8_t len);

#endif

/*
		libSPI.h - header
	Author:	Tinkerer, modified by Sky-WaLkeR
	
	Version: 1.2
	   Date: 20.06.2013 18:32

	Description:
  This library allows you to easily use hardware SPI module. 

	Depends:
  None

	Notes:
  This file must be in $(project_folder)\inc folder (like "spi_test\inc\libSPI.h")
  File libSPI.cpp must be in $(project_folder)\lib folder (like "spi_test\lib\libSPI.cpp")
  If you don't like this - change path in libSPI.cpp file
  
*/

#ifndef _LIB_SPI_HARDWARE_H_
#define _LIB_SPI_HARDWARE_H_

#include <avr/io.h>


// Pin settings
// Comment #error line to remove error
#error You may forget to configure pins! (See line before)
#define PORT_SPI	PORTB
#define DDR_SPI		DDRB
#define DD_MISO		4
#define DD_MOSI		3
#define DD_SS		2
#define DD_SCK		5

// Initialize pins for spi communication
void spi_init();

// Shift full array through target device
void spi_transfer_sync (uint8_t * dataout, uint8_t * datain, uint8_t len);

// Shift full array to target device without receiving any byte
void spi_transmit_sync (uint8_t *dataout, uint8_t len);

// Clocks one byte to target device and returns the received one
uint8_t spi_fast_shift (uint8_t data);

#endif